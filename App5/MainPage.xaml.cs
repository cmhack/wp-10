﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace App5
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Popup popup = new Popup();
        RootObject mListaClientes = new RootObject();
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.

            loadData();
        }

        public async void loadData() { 
             var client = new HttpClient();
            var response = await client.GetAsync( new Uri("http://204.93.178.91/~ddesign/IOS/Get.php"));           
            var result = await response.Content.ReadAsStringAsync();
            mListaClientes = JsonConvert.DeserializeObject<RootObject>(result);

            Debug.WriteLine(result);

            mList.DataContext = mListaClientes.data;
        }

        private void mButton_Click(object sender, RoutedEventArgs e)
        {
            /* Nueva instancia del USERCONTROL */
            newRowUserControl control = new newRowUserControl();
            
            /* Valores para el objeto Popup */
            popup.Height = 600;
            popup.Width = 410;
            popup.VerticalOffset = 30;

            /* Limpiamos los campos del usercontrol por seguridad y lo asignamos al Popup */
            control.resetForm();
            popup.Child = control;

            /* Asigna Delegados para saber lo que ocurre internamente en el USERCONTROL (newRowUserControl)*/
            control.closeClickEvent += control_closeClick;
            control.registerClickEvent += control_registerClick;
            control.emptyFieldsEvent += control_emptyFields;

            /* Mostramos el Objeto Popup */
            popup.IsOpen = true;
        }

        /* FUNCIONES CREADAS MANUALMENTE PARA MANEJAR LOS DELEGADOS DEL USERCONTROL */
        private void control_emptyFields()
        {
            throw new NotImplementedException();
        }

        private void control_registerClick(FormUrlEncodedContent encodedContent,Cliente nuevoCliente)
        {
          /* Se envia a otra funcion para que pueda ejecutarse de manera asincrona */
          this.sendToPost(encodedContent,nuevoCliente);
        }

        private void control_closeClick()
        {
            /* Cerrar el Popup, al dar click en el boton cerrar del USERCONTROL */
            popup.IsOpen = false;
        }

        private async void sendToPost(FormUrlEncodedContent encodedContent,Cliente nuevoCliente){
            /*
             * Aqui sucede la magia, se envian los datos al servidor. Previamente generados desde el USERCONTROL 
             * En resumen se ahorran varias lineas de codigo. Y la estructura de la aplicacion es facilmente escalable.
             * Clases, encapsulamiento y delegados son la ley! 
             * ¿Any questions? dev.cmedina@gmail.com
             */
            var client = new HttpClient();
            var response = await client.PostAsync(new Uri("http://204.93.178.91/~ddesign/IOS/Add.php"), encodedContent);
            var resu = await response.Content.ReadAsStringAsync();
            Debug.WriteLine("DATA " + resu);
            if (resu == "1")
            {
                 MessageDialog messageDialog = new MessageDialog("Registro Agregado Satisfactoriamente");
                 await messageDialog.ShowAsync();
                 mListaClientes.data.Add(nuevoCliente);
                 //((List<Cliente>)mList.DataContext).Add(nuevoCliente);
                 mList.DataContext = mListaClientes;
              
                 popup.IsOpen = false;
            }
            else
            {
                MessageDialog messageDialog = new MessageDialog("Ocurrio un error");
                await messageDialog.ShowAsync();
            }
        }
    }
}

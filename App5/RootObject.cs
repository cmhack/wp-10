﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App5
{
    public class Cliente
    {
        public string id { get; set; }
        public string rfc { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string credito { get; set; }
        public string ubicacion { get; set; }
        public string foto { get; set; }

       
        public Cliente(string _rfc, string _nombre, string _direccion, string _credito)
        {
            this.rfc = _rfc;
            this.nombre = _nombre;
            this.credito = _credito;
            this.ubicacion = _direccion;
            this.direccion = _direccion;
        }
    }

    public class RootObject
    {
        public List<Cliente> data { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace App5
{
    /* Declaracion de DELEGATES*/
    public delegate void CloseClick();
    public delegate void RegisterClick(FormUrlEncodedContent encodedContent, Cliente nuevoCliente);
    public delegate void EmptyFields();

    public sealed partial class newRowUserControl : UserControl
    {
        /* Objetos que manejaran los delegados */
        public event CloseClick closeClickEvent;
        public event RegisterClick registerClickEvent;
        public event EmptyFields emptyFieldsEvent;
        public newRowUserControl()
        {
            this.InitializeComponent();
        }

        private FormUrlEncodedContent getFormContent() { 
            /* 
             * Usamos una funcion para generar el FormUrlEncodedContent de forma encapsulada
             * Ya que los controles internos de un USERCONTROL no son accesibles desde fuera de la instancia misma.
             */

            var formContent = new FormUrlEncodedContent(new[]{
                        new KeyValuePair<string,string>("rfc",txtRFC.Text),
                        new KeyValuePair<string,string>("nombre",txtNombre.Text),
                        new KeyValuePair<string,string>("direccion",txtDireccion.Text),
                        new KeyValuePair<string,string>("credito",txtCredito.Text),
                        new KeyValuePair<string,string>("ubicacion",txtDireccion.Text)
                    });

            return formContent;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            /*
             * Un Delegate para manejar el evento Close del Control
             */
            var myEvent = this.closeClickEvent;
            if (myEvent != null)
                myEvent();
        }

        private void btnRegister_Click(object sender, RoutedEventArgs e)
        {
            /*
             * Al darle click al boton Registrar se validan los campos
             * Si estan vacios, se ejecuta el Delegate EMPTYFIELDS
             * de lo contrario sera REGISTERCLICK
             * (Los DELEGATES podriamos mencionarlos como LISTENERS para los eventos)
             */

            if (!string.IsNullOrWhiteSpace(txtNombre.Text) &&
                !string.IsNullOrWhiteSpace(txtRFC.Text) &&
                !string.IsNullOrWhiteSpace(txtDireccion.Text) &&
                !string.IsNullOrWhiteSpace(txtCredito.Text))
            {

                var myEvent = this.registerClickEvent;
                if (myEvent != null) /* Validamos que el delegate tenga algun listener asignado */
                    myEvent(this.getFormContent(), this.Cliente); /* Invocamos el delegate y le pasamos  parametros esperados */
                /* Aqui es solo para ejemplificar que se puede usar una funcion que retorne un valor y una propiedad (GETTER)*/
            }
            else
            {
                var myErrorEvent = this.emptyFieldsEvent;
                if (myErrorEvent != null) /* Validamos que el delegate tenga algun listener asignado */
                    myErrorEvent();
            }
        }

        public void resetForm()
        {
            txtCredito.Text = "";
            txtNombre.Text = "";
            txtRFC.Text = "";
            txtDireccion.Text = "";
        }

        public Cliente Cliente { 
            /* 
             * PROPERTY - GETTER
             * Genero un nuevo objeto cliente de nuevo encapsulado, para agregarlo a la lista de clientes actuales 
             * sin la necesiadad de recargar el JSON
             */
            get{
                return new Cliente(txtRFC.Text, txtNombre.Text, txtDireccion.Text, txtCredito.Text);
            }
        }
    }
}
